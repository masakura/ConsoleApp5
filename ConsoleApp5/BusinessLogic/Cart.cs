﻿using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp5.BusinessLogic
{
    internal sealed class Cart
    {
        public Cart(IEnumerable<CartItemRow> rows)
        {
            Rows = rows.ToArray();
        }

        public IEnumerable<CartItemRow> Rows { get; }
        public int TotalPrice => Rows.Sum(row => row.Price);
    }
}