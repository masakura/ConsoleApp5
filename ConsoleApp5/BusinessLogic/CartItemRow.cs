﻿using ConsoleApp5.DataAccess;

namespace ConsoleApp5.BusinessLogic
{
    internal sealed class CartItemRow
    {
        private readonly Item _item;

        public CartItemRow(Item item, int quantity)
        {
            _item = item;
            Quantity = quantity;
        }

        public int ItemId => _item.Id;
        public string ItemName => _item.Name;
        public int ItemUnitPrice => _item.Price;
        public int Quantity { get; }
        public int Price => _item.Price * Quantity;
    }
}