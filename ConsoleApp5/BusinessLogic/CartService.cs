﻿using System.Collections.Generic;
using ConsoleApp5.DataAccess;

namespace ConsoleApp5.BusinessLogic
{
    internal sealed class CartService
    {
        private readonly AppData _appData = new AppData();

        public Cart GetCart()
        {
            var items = _appData.GetCartItems();
            var rows = new List<CartItemRow>();

            foreach (var item in items)
            {
                rows.Add(new CartItemRow(_appData.GetItem(item.ItemId), item.Quantity));
            }

            return new Cart(rows);
        }
    }
}