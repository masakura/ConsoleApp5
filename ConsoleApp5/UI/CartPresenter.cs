﻿using System;
using ConsoleApp5.BusinessLogic;

namespace ConsoleApp5.UI
{
    internal sealed class CartPresenter
    {
        private readonly CartService _cartService = new CartService();

        public void DisplayCart()
        {
            var cart = _cartService.GetCart();

            Console.WriteLine("| Name | Unit price | Quantity | price |");
            Console.WriteLine("| ---- | ---------- | -------- | ----- |");

            foreach (var row in cart.Rows)
            {
                Console.WriteLine($"| {row.ItemName} | ${row.ItemUnitPrice} | ${row.Quantity} | ${row.Price} |");
            }
            
            Console.WriteLine("| ---------------------------- | ----- |");
            Console.WriteLine($"| Total                        | {cart.TotalPrice}");
        }
    }
}