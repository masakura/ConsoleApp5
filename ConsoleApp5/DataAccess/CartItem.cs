﻿namespace ConsoleApp5.DataAccess
{
    internal struct CartItem
    {
        public int ItemId { get; }
        public int Quantity { get; }

        public CartItem(int itemId, int quantity)
        {
            ItemId = itemId;
            Quantity = quantity;
        }
    }
}