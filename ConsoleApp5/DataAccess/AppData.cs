﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ConsoleApp5.DataAccess
{
    internal sealed class AppData
    {
        private readonly IDbConnection _connection;

        public AppData():this(new SqlConnection(""))
        {
        }

        public AppData(IDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<CartItem> GetCartItems()
        {
            var list = new List<CartItem>();
            
            using (var command = _connection.CreateCommand())
            {
                command.CommandText = "select ItemId, Quantity from CartItems";
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new CartItem(reader.GetInt32(0), reader.GetInt32(1)));
                    }
                }
            }

            return list.AsReadOnly();
        }

        public Item GetItem(int id)
        {
            using (var command = _connection.CreateCommand())
            {
                command.CommandText = "select Id, Name, Price from Item where Id = @Id";
                command.Parameters["Id"] = id;

                using (var reader = command.ExecuteReader())
                {
                    if (!reader.Read()) return null;
                    
                    return new Item(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2));
                }
            }
        }
    }
}