﻿using ConsoleApp5.UI;

namespace ConsoleApp5
{
    internal static class Program
    {
        private static void Main()
        {
            var cart = new CartPresenter();
            
            cart.DisplayCart();
        }
    }
}